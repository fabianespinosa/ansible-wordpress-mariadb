# Mariadb & Wordpress Role

Role for Ansible to install Wordpress (through EPEL in EL7) and to secure MariaDB with Vagrant. Based on the answers from this thread in [StackOverflow](http://stackoverflow.com/questions/16444306/ansible-idempotent-mysql-installation-playbook)

## Requirements

- LAMP Server

## Role Variables

If no variables are set, applying this role will result in a configuration equivalent to the default install. Consequently, no variables are required.

| Variable              | Default               | Comments (type)       |
| :---                  | :---                  | :---                  |
| `wp_db_name`          | 'database_name_here'  |                       |
| `wp_db_user`          | 'username_here'       |                       |
| `wp_db_password`      | 'password_here'       |                       |
| `wp_db_host`          | 'localhost'           |                       |
| `wp_db_charset`       | 'utf8'                |                       |
| `wp_db_collate`       | ''                    |                       |
| `wp_table_prefix`     | 'wp_'                 |                       |
| `wp_debug`            | false                 |                       |
| `wp_fs_method`        | 'direct'              | Allowed values: `php` |
| `wp_lang`             | ''                    |                       |
| `mysql_root_password` | set_password          |                       |

## License

Public Domain

## Author Information

Fabián Espinosa-Díaz (fabianespinosa@gmail.com)

